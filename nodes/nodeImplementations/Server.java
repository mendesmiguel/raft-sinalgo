package projects.raft.nodes.nodeImplementations;


import projects.raft.CustomGlobal;
import projects.raft.Logger;
import projects.raft.nodes.messages.AppendEntriesReply;
import projects.raft.nodes.messages.AppendEntriesRequest;
import projects.raft.nodes.messages.RequestVoteReply;
import projects.raft.nodes.messages.RequestVoteRequest;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.transformation.PositionTransformation;
import sinalgo.nodes.Node;
import sinalgo.nodes.edges.Edge;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;
import sinalgo.runtime.Global;
import sinalgo.runtime.Runtime;
import sinalgo.tools.Tools;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Server extends Node {
    private final boolean CONSOLE_ON = false;
    private final Logger LOGGER = new Logger(false, CONSOLE_ON);

    private State state;
    private int currentTerm;
    private int votedFor;
    private int commitIndex;
    private int leader;
    private int numOfNodes = 0;
    private int deltaElection;
    private int electionTimeOut;
    private List<Integer> log;
    private int votes;
    private int roundsToSendNextHeartBeat;

    private int[] nextIndex = null;
    private int[] matchIndex = null;

    @Override
    public void init() {
        this.setColor(Color.ORANGE);

        state = State.FOLLOWER;
        currentTerm = 1;
        votedFor = -1;
        commitIndex = 0;
        leader = -1;
        deltaElection = randomInt(10, 51);
        electionTimeOut = deltaElection;
        log = new ArrayList<>();
        leader = -1;
        votes = 0;
        roundsToSendNextHeartBeat = 5;

        LOGGER.log(this, " sets electionTimeOut to " + electionTimeOut);
    }

    @Override
    public void preStep() {
        simulateRequest();
        onElectionStartTimeOut();
        updateCommitIndex();
    }

    private void updateCommitIndex() {
        if (matchIndex != null) {
            int[] matchIndexCopy = Arrays.copyOf(matchIndex, matchIndex.length);
            Arrays.sort(matchIndexCopy);

            int n = matchIndexCopy[(int) Math.floor(numOfNodes / 2)];
            if (state == State.LEADER && logTerm(n) == currentTerm)
                commitIndex = Math.max(commitIndex, n);
        }
    }

    private void simulateRequest() {
        if (state == State.LEADER) {
            if (randomInt(1, 21) < 3) {
                log.add(currentTerm);
                LOGGER.log(this, "has received a request from CLIENT - appended: " + currentTerm + " to log: " + log);
            }
        }
    }

    private void onElectionStartTimeOut() {
        boolean timeOut = electionTimeOut == now();
        if (timeOut) {
            if (state == State.FOLLOWER || state == State.CANDIDATE) {
                LOGGER.log(this, "Election TIMEOUT!!! Starts new election.");
                if (state == State.FOLLOWER) {
                    state = State.CANDIDATE;
                    LOGGER.log(this, "has become CANDIDATE");
                }
                int prevElecTimeOut = electionTimeOut;
                electionTimeOut = now() + roundsToSendNextHeartBeat + randomInt(1, 5);
                LOGGER.log(this, "resets its electionTimeOut from " + prevElecTimeOut + " to " + electionTimeOut);

                currentTerm += 1;

                votedFor = this.ID;
                votes = 1;

                for (Edge edge : this.outgoingConnections) {
                    Node neighbour = edge.endNode;

                    if (neighbour != null) {
                        RequestVoteRequest requestVoteRequest = new RequestVoteRequest(this, neighbour,
                                currentTerm, logTerm(log.size()), log.size());
                        LOGGER.log(this, "sends a " + requestVoteRequest + " to " + neighbour);
                        this.send(requestVoteRequest, neighbour);
                    }
                }
            } else if (state == State.LEADER) {
                int prevElecTimeOut = electionTimeOut;
                electionTimeOut = now() + roundsToSendNextHeartBeat;
                LOGGER.log(this, "HeartBeat TIMEOUT!!! Sends heartbeats and resets its electionTimeOut from "
                        + prevElecTimeOut + " to " + electionTimeOut);
                sendAppendEntries();
            }
        }
    }

    private int lastLogTerm() {
        if (log.size() == 0)
            return 0;
        return log.get(log.size() - 1);
    }

    @Override
    public void handleMessages(Inbox inbox) {
        for (Message message : inbox) {
            if (message instanceof RequestVoteRequest) {
                RequestVoteRequest requestVoteRequest = (RequestVoteRequest) message;
                LOGGER.log(this, "received a RequestVoteRequest message from " + requestVoteRequest.from());
                handleRequestVoteRequest(requestVoteRequest);
            } else if (message instanceof RequestVoteReply) {
                RequestVoteReply requestVoteReply = (RequestVoteReply) message;
                LOGGER.log(this, "received a RequestVoteReply message from " + requestVoteReply.from()
                        + " granted: " + requestVoteReply.isGranted());
                handleRequestVoteReply(requestVoteReply);
            } else if (message instanceof AppendEntriesRequest) {
                AppendEntriesRequest appendEntriesRequest = (AppendEntriesRequest) message;
                LOGGER.log(this, "received a AppendEntriesRequest message from " + appendEntriesRequest.from());
                handleAppendEntriesRequest(appendEntriesRequest);
            } else if (message instanceof AppendEntriesReply) {
                AppendEntriesReply appendEntriesReply = (AppendEntriesReply) message;
                LOGGER.log(this, "received a AppendEntriesReply message from " + appendEntriesReply.from());
                handleAppendEntriesReply(appendEntriesReply);
            } else {
                LOGGER.log(this, "received an unsupported message");
            }
        }
    }

    private void handleRequestVoteRequest(RequestVoteRequest request) {
        if (currentTerm < request.term())
            stepDown(request.term());

        boolean granted = false;
        LOGGER.log(this, "currentTerm: " + currentTerm + " request.term(): " + request.term() +
                " votedFor: " + votedFor + " request.lastLogTerm(): " + request.lastLogTerm() + " logTerm(log.size()): " +
                logTerm(log.size()) + " request.lastLogTerm() " + request.lastLogTerm() + " request.lastLogIndex(): " +
                request.lastLogIndex() + " log.size(): " + log.size());
        if (currentTerm == request.term() &&
                (votedFor == -1 || votedFor == request.from().ID) &&
                (request.lastLogTerm() > logTerm(log.size()) ||
                        (request.lastLogTerm() == logTerm(log.size()) &&
                                request.lastLogIndex() >= log.size()))) {
            granted = true;
            votedFor = request.from().ID;
            electionTimeOut = makeElectionAlarm(now());
            LOGGER.log(this, "votedFor: " + votedFor + " sets new electionTimeOut: " + electionTimeOut);
        }

        RequestVoteReply requestVoteReply = new RequestVoteReply(this, request.from(), currentTerm, granted);
        send(requestVoteReply, requestVoteReply.to());
    }

    private void handleRequestVoteReply(RequestVoteReply requestVoteReply) {
        if (state == State.CANDIDATE && requestVoteReply.isGranted()) {
            votes++;

            if (votes > Math.floor(numOfNodes / 2)) {
                CustomGlobal.numOfLeaders++;
                LOGGER.log(this, "is the new LEADER - received " + votes + " of a total of " + numOfNodes);
                state = State.LEADER;
                leader = this.ID;
                votes = 0;
                Arrays.fill(nextIndex, log.size() + 1);
                sendAppendEntries();
            }
        }
    }

    private void handleAppendEntriesRequest(AppendEntriesRequest request) {

        boolean success = false;
        int matchIndex = 0;

        if (currentTerm < request.term())
            stepDown(request.term());
        if (currentTerm == request.term()) {
            state = State.FOLLOWER;
            electionTimeOut = makeElectionAlarm(now());

            if (request.prevIndex() == 0 || (request.prevIndex() <= log.size() &&
                    logTerm(request.prevIndex()) == request.prevTerm())) {
                success = true;
                int index = request.prevIndex();

                for (int i = 0; i < request.entries().size(); i++) {
                    index++;
                    if (logTerm(index) != request.entries().get(i)) {
                        while (log.size() > index - 1) {
                            LOGGER.log(this, "will remove: " + log.get(log.size() - 1) + " from log: " + log);
                            log.remove(log.size() - 1);
                        }
                        log.add(request.entries().get(i));
                    }
                }
                matchIndex = index;
                commitIndex = Math.max(commitIndex, request.commitIndex());
               // System.out.println("CommitIndex: " + commitIndex + " round: " + CustomGlobal.rounds);
            }
        }
        AppendEntriesReply reply = new AppendEntriesReply(this, request.from(), currentTerm, success, matchIndex);
        LOGGER.log(this, "sends a " + reply + " to " + reply.to() + " current log: " + log);
        send(reply, reply.to());
    }

    private void handleAppendEntriesReply(AppendEntriesReply reply) {
        LOGGER.log(this, "is handling a " + reply + " from " + reply.from());

        if (currentTerm < reply.term())
            stepDown(reply.term());
        if (state == State.LEADER && currentTerm == reply.term()) {
            int replyFromID = reply.from().ID - 1;
            if (reply.success()) {
                matchIndex[replyFromID] = Math.max(matchIndex[replyFromID], reply.matchIndex());
                nextIndex[replyFromID] = reply.matchIndex() + 1;
            } else {
                nextIndex[replyFromID] = Math.max(1, nextIndex[replyFromID] - 1);
            }
        }
    }

    private void sendAppendEntries() {
        for (Edge edge : this.outgoingConnections) {
            Node peer = edge.endNode;

            if (peer != null) {
                int peerIndex = peer.ID - 1;
                if (state == State.LEADER) {   //  && nextIndex[peerIndex] <= log.size()) {
                    int prevIndex = nextIndex[peerIndex] - 1;
                    int lastIndex = log.size();

                    if (matchIndex[peerIndex] + 1 < nextIndex[peerIndex])
                        lastIndex = prevIndex;

                    AppendEntriesRequest appendEntriesRequest = new AppendEntriesRequest(this, peer,
                            currentTerm, prevIndex, logTerm(prevIndex), makeEntries(prevIndex, lastIndex),
                            Math.min(commitIndex, lastIndex));

                    LOGGER.log(this, "sends a " + appendEntriesRequest + " to " + peer);

                    this.send(appendEntriesRequest, peer);
                }
            }
        }

    }

    private List<Integer> makeEntries(int prevIndex, int lastIndex) {
        return new ArrayList<>(log.subList(prevIndex, lastIndex));
    }

    private int logTerm(int index) {
        if (index < 1 || index > log.size()) {
            return 0;
        } else {
            return log.get(index - 1);
        }
    }

    private void stepDown(int term) {
        LOGGER.log(this, "stepDown - prevState: " + state +
                " new State: FOLLOWER - prevTerm: " + currentTerm +
                " new term: " + term);
        currentTerm = term;
        state = State.FOLLOWER;
        votedFor = -1;
        votes = 0;

        if (this.electionTimeOut <= now()) {
            int prevTimeOut = this.electionTimeOut;
            this.electionTimeOut = makeElectionAlarm(now());
            LOGGER.log(this, "resetting electionTimeOut from: " + prevTimeOut + " to: " + this.electionTimeOut);
        }
    }

    private int makeElectionAlarm(int now) {
        int newElectionTimeOut = now + electionTimeoutOffset();
        return newElectionTimeOut;
    }

    private int electionTimeoutOffset() {
        return (int) randomDouble(1, 2) * deltaElection;
    }

    private int now() {
        return (int) Global.currentTime;
    }

    @Override
    public void neighborhoodChange() {

    }

    public int getCommitIndex() {
        return commitIndex;
    }

    @Override
    public void postStep() {
        CustomGlobal.messages += Global.numberOfMessagesInThisRound;
        CustomGlobal.rounds++;
        numOfNodes = Math.max(numOfNodes, Runtime.nodes.size());

        if (nextIndex == null && matchIndex == null)
            initIndexes();

        if (now() % 10 == 0) {
            printLog();
        }
        if (CONSOLE_ON && this.ID == Tools.getNodeList().size()) {
            System.out.println("ROUND " + now() + " has ended!");
        }
    }

    private void printLog() {
        LOGGER.log(this, "log: " + log);
    }

    @Override
    public void draw(Graphics g, PositionTransformation pt, boolean highlight) {
        this.drawAsDisk(g, pt, highlight, this.drawingSizeInPixels);
        this.drawNodeAsDiskWithText(g, pt, highlight, String.valueOf(this.ID), 20, Color.BLACK);
        if (state == State.FOLLOWER) {
            this.setColor(Color.ORANGE);
        } else if (state == State.CANDIDATE) {
            this.setColor(Color.BLUE);
        } else {
            this.setColor(Color.RED);
        }
    }

    @Override
    public void checkRequirements() throws WrongConfigurationException {

    }

    public List<Integer> log() {
        return log;
    }

    public State state() {
        return state;
    }

    @Override
    public String toString() {
        return state + "(ID:" + this.ID + "|T:" + currentTerm + ")";
    }

    private void initIndexes() {
        if (numOfNodes > 0) {
            nextIndex = new int[numOfNodes];
            matchIndex = new int[numOfNodes];

            Arrays.fill(matchIndex, 0);
            Arrays.fill(nextIndex, 1);
        }
    }

    private double randomDouble(int min, int max) {
        return ThreadLocalRandom.current().nextDouble(min, max + 1);
    }

    private int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public enum State {
        FOLLOWER, CANDIDATE, LEADER
    }

}
