package projects.raft.nodes.messages;

import sinalgo.nodes.Node;

import java.util.ArrayList;
import java.util.List;

public class AppendEntriesRequest extends AbstractMessage {
    private int prevIndex;
    private int prevTerm;
    private List<Integer> entries;
    private int commitIndex;

    public AppendEntriesRequest(Node from, Node to, int term, int prevIndex, int prevTerm, List<Integer> entries, int commitIndex) {
        super(from, to, term);
        this.prevIndex = prevIndex;
        this.prevTerm = prevTerm;
        this.entries = entries;
        this.commitIndex = commitIndex;
    }

    public int prevIndex() {
        return prevIndex;
    }

    public int prevTerm() {
        return prevTerm;
    }

    public List<Integer> entries() {
        return new ArrayList<>(entries);
    }

    public int commitIndex() {
        return commitIndex;
    }

    @Override
    public String toString() {
        return "AppendEntriesRequest(prevIndex: " + prevIndex + " | prevTerm: " + prevTerm +
                " | commitIndex: " + commitIndex + " | entries: " + entries + ")";
    }
}
