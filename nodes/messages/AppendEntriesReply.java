package projects.raft.nodes.messages;

import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

public class AppendEntriesReply extends AbstractMessage {

    private final boolean success;
    private final int matchIndex;

    public AppendEntriesReply(Node from, Node to, int term, boolean success, int matchIndex) {
        super(from, to, term);
        this.success = success;
        this.matchIndex = matchIndex;
    }

    public boolean success() {
        return success;
    }

    public int matchIndex() {
        return matchIndex;
    }

    @Override
    public Message clone() {
        return this;
    }

    @Override
    public String toString() {
        return "AppendEntriesReply(success: " + success + " | matchIndex: " + matchIndex + ")";
    }

}
