package projects.raft.nodes.messages;


import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

public class AbstractMessage extends Message {
    private Node from, to;
    private int term;

    public AbstractMessage(Node from, Node to, int term) {
        this.from = from;
        this.to = to;
        this.term = term;
    }

    public Node from() {
        return from;
    }

    public Node to() {
        return to;
    }

    public int term() {
        return term;
    }

    @Override
    public Message clone() {
        return this;
    }
}
