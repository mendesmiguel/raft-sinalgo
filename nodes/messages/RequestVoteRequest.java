package projects.raft.nodes.messages;

import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

public class RequestVoteRequest extends AbstractMessage {
    private int lastLogIndex;
    private int lastLogTerm;

    public RequestVoteRequest(Node from, Node to, int term, int lastLogTerm, int lastLogIndex) {
        super(from, to, term);
        this.lastLogIndex = lastLogIndex;
        this.lastLogTerm = lastLogTerm;
    }

    @Override
    public Message clone() {
        return this;
    }

    @Override
    public String toString() {
        return "RequestVoteRequest(lastLogTerm: " + lastLogTerm + " | lastLogIndex: " + lastLogIndex + ")";
    }


    public int lastLogIndex() {
        return lastLogIndex;
    }

    public int lastLogTerm() {
        return lastLogTerm;
    }

}
