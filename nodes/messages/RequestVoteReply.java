package projects.raft.nodes.messages;

import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

public class RequestVoteReply extends AbstractMessage {
    private boolean granted;

    public RequestVoteReply(Node from, Node to, int term, boolean granted) {
        super(from, to, term);
        this.granted = granted;
    }

    public boolean isGranted() {
        return granted;
    }

    @Override
    public Message clone() {
        return this;
    }

    @Override
    public String toString() {
        return "RequestVoteReply";
    }
}
