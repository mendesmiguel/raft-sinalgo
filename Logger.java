package projects.raft;


import sinalgo.nodes.Node;
import sinalgo.runtime.Global;

public class Logger {

    private final boolean saveToFile;
    private final boolean showOnConsole;

    public Logger(boolean saveToFile, boolean showOnConsole) {
        this.saveToFile = saveToFile;
        this.showOnConsole = showOnConsole;
    }

    public Logger() {
        this(false, true);
    }

    public void log(Node node, String message) {
        if (showOnConsole)
            System.out.println("ROUND " + (int) (Math.round(Global.currentTime)) + " - " + node + " " + message);
    }

    public void logError(Node node, String message) {
        if (showOnConsole)
            System.out.println("ROUND " + (int) (Math.round(Global.currentTime)) + " - ERROR: " + node.ID + " " + message);
    }

    public void logOnFile() {

    }
}
